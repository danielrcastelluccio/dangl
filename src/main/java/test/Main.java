package test;

import dangl.DanGL;
import dangl.audio.AudioManager;
import dangl.display.Display;
import dangl.display.DisplayManager;
import dangl.input.keyboard.Key;
import dangl.input.keyboard.Keyboard;
import dangl.loop.AbstractLoop;
import dangl.loop.LoopManager;
import dangl.render.*;
import dangl.update.SceneObject;
import dangl.update.ObjectHandler;
import dangl.util.Resource;
import jogamp.graph.curve.tess.Loop;

import java.awt.*;

public class Main extends AbstractLoop {

    public static double deltaX = 0, deltaY = 0;

    SceneObject player;

    Resource resource2 = new Resource("src/main/res/song.wav");
    Resource resource = new Resource("src/main/res/Fade.wav");

    ImageResource image = new ImageResource(new Resource("src/main/res/images/character.png"));

    public static Display main;
    public static Display off;

    public static void main(String[] args) {

        DanGL.addSettings(new DanGLSettings());
        main = new Display("Yeet", 1600, 900);
        DisplayManager.addDisplay(main);
        off = new Display("Poopy", 1600, 900);
        DisplayManager.addDisplay(off);

        main.create();

        DanGL.begin();
    }

    public void init() {

        player = new SceneObject(main.getWidth() / 2, main.getHeight() / 2 - 300, 200, 400, 20, 10, new ImageResource(new Resource("src/main/res/images/character.png")));
        //ObjectHandler.addObject(player);
        //ObjectHandler.addObject(new SceneObject(main.getWidth() / 2 + 400, main.getHeight() / 2 - 300, 200, 400, 0, 10, new ImageResource(new Resource("src/main/res/images/character.png"))));
        //ObjectHandler.addObject(new SceneObject(0, main.getWidth() / 2 + 300, main.getWidth(), 200, 0, 0, new GLColor(Color.GREEN)));

        /*AudioManager.playSound(resource2);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        AudioManager.playSound(resource);*/

    }

    @Override
    public void update(double deltaTime) {

        deltaX = 0;
        deltaY = 0;

        if(Keyboard.isKeyDown(Key.A)) {
            deltaX = -5;
        }

        if(Keyboard.isKeyDown(Key.D)) {
            deltaX = 5;
        }

        if(Keyboard.isKeyDown(Key.W)) {
            deltaY = -5;
        }

        if(Key.SPACE.isKeyPressed()) {

            if(!off.isAlive()) {
                off.create();
            } else {
                off.destroy();
            }

        }

    }

    @Override
    public void render() {

        RenderQueue.drawRectSpecificScreen(0, 0, 50, 50, 45, off, new GLColor(Color.RED));
        RenderQueue.drawImage(0, 0, 100, 100, 0, image);

        System.out.println(DanGL.getFPS());

    }

    public void exit() {



    }
}
