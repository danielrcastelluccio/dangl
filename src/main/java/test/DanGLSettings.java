package test;

import dangl.AbstractSettings;
import dangl.display.Display;
import dangl.loop.AbstractLoop;

public class DanGLSettings extends AbstractSettings {

    @Override
    public Display getMainDisplay() {
        return Main.main;
    }

    @Override
    public Class<? extends AbstractLoop> getLoopClass() {
        return Main.class;
    }

    @Override
    public int getTickRate() {
        return 120;
    }

}
