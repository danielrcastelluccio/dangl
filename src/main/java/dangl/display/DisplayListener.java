package dangl.display;

import com.jogamp.opengl.*;
import com.sun.prism.impl.BufferUtil;
import dangl.loop.LoopManager;
import dangl.render.GLColor;
import dangl.render.GLObject;
import dangl.render.Renderer;

import java.awt.*;
import java.util.Iterator;

public class DisplayListener implements GLEventListener {

    public GL2 gl;

    static int i = 0;

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {

        gl = glAutoDrawable.getGL().getGL2();

        gl.glEnable(GL2.GL_TEXTURE_2D);

        gl.glEnable(GL2.GL_BLEND);
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);

        gl.setSwapInterval(0);

    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {

        try {

            for (Display display : DisplayManager.getDisplays()) {

                if (this.equals(display.listener) && display.isAlive()) {

                    for (GLObject object : Renderer.objects.get(display)) {
                        object.render(gl);
                    }

                    if (!display.updatedSize) {
                        gl.glMatrixMode(GL2.GL_PROJECTION);
                        gl.glLoadIdentity();
                        gl.glOrtho(0, display.getWidth(), -display.getHeight(), 0, -1, 1);
                        gl.glMatrixMode(GL2.GL_MODELVIEW);
                        display.updatedSize = true;
                    }

                }

            }

        } catch(Exception e) { }

    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {

        for(Display display : DisplayManager.getDisplays()) {
            if(this.equals(display.listener)) {
                display.updatedSize = false;
            }
        }
    }

}
