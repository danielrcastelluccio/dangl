package dangl.display;

import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLProfile;
import dangl.input.keyboard.KeyboardListener;
import dangl.input.mouse.MouseListener;
import dangl.render.GLObject;
import dangl.render.Renderer;

import java.util.ArrayList;
import java.util.Arrays;

public class Display {

    private GLWindow window;
    private GLProfile profile;

    public boolean updatedSize = false;

    public DisplayListener listener;

    private String startTitle;
    private int startWidth;
    private int startHeight;

    public Display(String title, int width, int height) {

        this.startTitle = title;
        this.startWidth = width;
        this.startHeight = height;

    }

    public void create() {

        Renderer.objects.put(this, new ArrayList<>());

        GLProfile.initSingleton();
        profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities caps = new GLCapabilities(profile);

        window = GLWindow.create(caps);
        window.setSize(startWidth, startHeight);
        window.setResizable(true);
        window.setTitle(startTitle);

        listener = new DisplayListener();

        window.addGLEventListener(listener);
        window.addKeyListener(KeyboardListener.getInstance());
        window.addMouseListener(MouseListener.getInstance());

        window.setVisible(true);


    }

    public void destroy() {
        window.setVisible(false);
        window.destroy();
    }

    public double getScaledWidth() {
        return (double) window.getWidth() / (double) startWidth;
    }

    public double getScaledHeight() {
        return (double) window.getHeight() / (double) startHeight;
    }

    public double getWidth(){
        return window.getWidth();
    }

    public double getHeight() {
        return window.getHeight();
    }

    public String getTitle() {
        return window.getTitle();
    }

    public GLProfile getProfile() {
        return profile;
    }

    public GLWindow getWindow() {
        return window;
    }

    public GLContext getContext() {
        return window.getContext();
    }

    public boolean isAlive() {
        return window != null && window.isVisible();
    }

}
