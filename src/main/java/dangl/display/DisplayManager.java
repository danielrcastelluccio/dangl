package dangl.display;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DisplayManager {

    private static ArrayList<Display> displays = new ArrayList<>();

    private static HashMap<Display, DisplayListener> displayListeners = new HashMap<>();

    public static void addDisplay(Display display) {
        displays.add(display);
    }

    public static ArrayList<Display> getDisplays() {
        return displays;
    }

}
