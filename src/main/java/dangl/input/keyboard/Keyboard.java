package dangl.input.keyboard;

public class Keyboard {

    public static boolean isKeyDown(Key key) {
        return key.isKeyDown();
    }

    public static boolean isKeyPressed(Key key) {
        return key.isKeyPressed();
    }

    public static boolean isKeyReleased(Key key) {
        return key.isKeyReleased();
    }

}
