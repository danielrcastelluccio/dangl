package dangl.input.keyboard;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.KeyListener;

import java.util.ArrayList;
import java.util.List;

public class KeyboardListener implements KeyListener {

    public List<Double> currentDownKeys = new ArrayList<>();
    public List<Double> currentPressedKeys = new ArrayList<>();
    public List<Double> currentReleasedKeys = new ArrayList<>();

    private static final KeyboardListener INSTANCE = new KeyboardListener();

    public static KeyboardListener getInstance() {
        return INSTANCE;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(!currentDownKeys.contains((double) e.getKeyCode())) {
            currentDownKeys.add((double) e.getKeyCode());
            currentPressedKeys.clear();
            currentPressedKeys.add((double) e.getKeyCode());
        } else {
            currentPressedKeys.remove((double) e.getKeyCode());
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

        if(e.isAutoRepeat()) {
            return;
        }

        currentDownKeys.remove((double) e.getKeyCode());
        currentPressedKeys.remove((double) e.getKeyCode());
        if(!currentReleasedKeys.contains((double) e.getKeyCode())) {
            currentReleasedKeys.add((double) e.getKeyCode());
        }

    }
}
