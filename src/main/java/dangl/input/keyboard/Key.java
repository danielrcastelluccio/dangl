package dangl.input.keyboard;

public enum Key {

    ESCAPE(27), F1(112), F2(113), F3(114), F4(115), F5(116), F6(117), F7(118), F8(119), F9(120), F10(121), F11(122), F12(123),

    TILDA(192), ONE_ROW(49), TWO_ROW(50), THREE_ROW(51), FOUR_ROW(52), FIVE_ROW(53), SIX_ROW(54), SEVEN_ROW(55), EIGHT_ROW(56), NINE_ROW(57), ZERO_ROW(48), MINUS(45), EQUALS(61), BACKSPACE(8),

    TAB(0), Q(81), W(87), E(69), R(82), T(84), Y(89), U(85), I(73), O(79), P(80), BRACKET_OPEN(97), BRACKET_CLOSE(93), SLASH_BACK(92),

    CAPS_LOCK(20), A(65), S(83), D(68), F(70), H(72), J(74), K(75), L(76), SEMI_COLON(59), QUOTATION(222), ENTER(10),

    SHIFT(16), Z(90), X(88), C(67), V(85), B(66), N(78), M(77), COMMA(44), PERIOD(46), SLASH_FORWARD(47),

    CONTROL(17), WINDOWS(524), ALT(18), SPACE(32), IDK(525);

    public int keyCode;

    Key(int keyCode) {
        this.keyCode = keyCode;
    }

    public synchronized boolean isKeyDown() {
        return KeyboardListener.getInstance().currentDownKeys.contains((double) this.keyCode);
    }

    public synchronized boolean isKeyPressed() {

        if(KeyboardListener.getInstance().currentPressedKeys.contains((double) this.keyCode)) {
            KeyboardListener.getInstance().currentPressedKeys.remove((double) this.keyCode);
            return true;
        } else {
            return false;
        }
    }

    public synchronized boolean isKeyReleased() {

        if(KeyboardListener.getInstance().currentReleasedKeys.contains((double) this.keyCode)) {
            KeyboardListener.getInstance().currentReleasedKeys.remove((double) this.keyCode);
            return true;
        } else {
            return false;
        }

    }

}
