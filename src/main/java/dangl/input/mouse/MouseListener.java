package dangl.input.mouse;

import com.jogamp.newt.event.MouseEvent;

import java.util.ArrayList;
import java.util.List;

public class MouseListener implements com.jogamp.newt.event.MouseListener {

    public List<Double> currentPressedButtons = new ArrayList<>();

    public int mouseX;
    public int mouseY;

    private static final MouseListener INSTANCE = new MouseListener();

    public static MouseListener getInstance() { return INSTANCE; }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(!currentPressedButtons.contains((double)e.getButton())) {
            currentPressedButtons.add((double) e.getButton());
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        currentPressedButtons.remove((double) e.getButton());
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
    }

    @Override
    public void mouseWheelMoved(MouseEvent mouseEvent) {

    }
}
