package dangl.input.mouse;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Mouse {

    public static boolean isButtonDown(int button) {
        return MouseListener.getInstance().currentPressedButtons.contains((double) button);
    }

    public static double getX() {
        return MouseListener.getInstance().mouseX;
    }

    public static double getY() {
        return MouseListener.getInstance().mouseY;
    }

    public static String getCurrentButtons() {
        return MouseListener.getInstance().currentPressedButtons.toString();
    }

}
