package dangl.input.mouse;

public class Button {

    public static final int LEFT_PRIMARY = 1;
    public static final int SCROLL = 1;
    public static final int RIGHT_PRIMARY = 3;
    public static final int BUTTON_4 = 4;
    public static final int BUTTON_5 = 5;
    public static final int BUTTON_6 = 6;
    public static final int BUTTON_7 = 7;
    public static final int BUTTON_8 = 8;
    public static final int BUTTON_9 = 9;

}
