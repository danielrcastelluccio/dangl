package dangl.render;

import java.awt.*;

public class GLColor {

    public float r;
    public float g;
    public float b;
    public float a;

    public GLColor(int r, int g, int b, int a) {
        this.r = r / 255;
        this.g = g / 255;
        this.b = b / 255;
        this.a = a / 255;
    }

    public GLColor(int r, int g, int b) {
        this.r = r / 255;
        this.g = g / 255;
        this.b = b / 255;
        this.a = 1;
    }

    public GLColor(float r, float g, float b, float a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    public GLColor(float r, float g, float b) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = 1;
    }

    public GLColor(Color color) {
        this.r = color.getRed() / 255;
        this.g = color.getGreen() / 255;
        this.b = color.getBlue() / 255;
        this.a = color.getAlpha() / 255;
    }

}
