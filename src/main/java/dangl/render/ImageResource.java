package dangl.render;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;
import dangl.DanGL;
import dangl.display.Display;
import dangl.util.Resource;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageResource {

    private Texture texture;

    private BufferedImage image;

    public ImageResource(Resource resource) {

        try {
            image = ImageIO.read(resource.file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(image != null) {
            image.flush();
        }

    }

    public Texture getTexture() {

        if(image == null) {
            return null;
        }

        if(texture == null) {
            texture = AWTTextureIO.newTexture(DanGL.getSettings().getMainDisplay().getProfile(), image, true);
        }

        return texture;
    }

}
