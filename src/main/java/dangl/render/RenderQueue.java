package dangl.render;

import dangl.DanGL;
import dangl.display.Display;

public class RenderQueue {

    public static void drawRect(double x, double y, double width, double height, double rotation, GLColor color) {
        DanGL.getRenderer().addObject(new GLObject(GLObject.ObjectType.RECT, x, y, width, height, rotation, color));
    }

    public static void drawRectSpecificScreen(double x, double y, double width, double height, double rotation, Display display, GLColor color) {
        DanGL.getRenderer().addObject(new GLObject(GLObject.ObjectType.RECT, x, y, width, height, rotation, display, color));
    }

    public static void drawImage(double x, double y, double width, double height, double rotation, ImageResource resource) {
        DanGL.getRenderer().addObject(new GLObject(GLObject.ObjectType.IMAGE, x, y, width, height, rotation, resource));
    }

    public static void drawImageSpecificScreen(double x, double y, double width, double height, double rotation, Display display, ImageResource resource) {
        DanGL.getRenderer().addObject(new GLObject(GLObject.ObjectType.IMAGE, x, y, width, height, rotation, display, resource));
    }

}
