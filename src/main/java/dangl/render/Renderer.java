package dangl.render;

import dangl.display.Display;
import dangl.display.DisplayManager;
import dangl.loop.LoopManager;

import java.util.ArrayList;
import java.util.HashMap;

public class Renderer {

    public static HashMap<Display, ArrayList<GLObject>> objects = new HashMap<>();

    public void addObject(GLObject glObject) {
        if(glObject.display.isAlive()) {
            objects.get(glObject.display).add(glObject);
        }
    }

    public void removeObject(GLObject glObject) {
        objects.get(glObject.display).remove(glObject);
    }

    public static void render() {

        boolean displaysAlive = false;

        for(Display display : DisplayManager.getDisplays()) {
            if(display.isAlive()) {
                display.getWindow().display();
                displaysAlive = true;
            }
        }

        if(!displaysAlive) {
            LoopManager.running = false;
        }

    }
}
