package dangl.render;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.util.texture.Texture;
import dangl.DanGL;
import dangl.display.Display;

import java.nio.Buffer;

import static com.jogamp.opengl.GL.GL_TRIANGLES;

public class GLObject {

    private ObjectType type;
    private float x;
    private float y;
    private float width;
    private float height;
    private float rotation;
    private Object[] args;
    public Display display;

    public static double translationPointX = 0;
    public static double translationPointY = 0;
    public static double rotationPoint = 0;

    public GLObject(ObjectType type, double x, double y, double width, double height, double rotation, Object... args) {
        this.type = type;
        this.x = (float) x;
        this.y = (float) y;
        this.width = (float) width;
        this.height = (float) height;
        this.rotation = (float) rotation;
        this.args = args;
        this.display = DanGL.getSettings().getMainDisplay();
    }

    public GLObject(ObjectType type, double x, double y, double width, double height, double rotation, Display display, Object... args) {
        this.type = type;
        this.x = (float) x;
        this.y = (float) y;
        this.width = (float) width;
        this.height = (float) height;
        this.rotation = (float) rotation;
        this.args = args;
        this.display = display;
    }

    public void moveTo(GL2 gl, double x, double y, double rot) {

        gl.glTranslatef((float) (x - translationPointX), (float) (y - translationPointY), 0);
        gl.glRotatef((float) (rot - rotationPoint), 0, 0, 1);

        translationPointX = x;
        translationPointY = y;
        rotationPoint = rot;

    }

    public void render(GL2 gl) {

        gl.glTranslatef(x + width / 2 - Camera.getCamera().getLocation().x, -y - height / 2 + Camera.getCamera().getLocation().y, 0);
        gl.glRotatef(-rotation, 0, 0, 1);
        gl.glTranslatef(-width / 2, height / 2, 0);

        switch(type) {

            case RECT:
                gl.glColor4f(((GLColor) args[0]).r, ((GLColor) args[0]).g, ((GLColor) args[0]).b, ((GLColor) args[0]).a);
                gl.glBegin(GL2.GL_QUADS);
                    gl.glVertex2f(0, -height);
                    gl.glVertex2f(width, -height);
                    gl.glVertex2f(width, 0);
                    gl.glVertex2f(0, 0);
                gl.glEnd();
                break;
            case IMAGE:
                gl.glColor3f(1, 1, 1);
                Texture tex = ((ImageResource) args[0]).getTexture();
                gl.glBindTexture(GL2.GL_TEXTURE_2D, tex.getTextureObject());
                gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
                gl.glBegin(GL2.GL_QUADS);
                    gl.glTexCoord2f(0, 1);
                    gl.glVertex2f(0, -height);
                    gl.glTexCoord2f(1, 1);
                    gl.glVertex2f(width, -height);
                    gl.glTexCoord2f(1, 0);
                    gl.glVertex2f(width, 0);
                    gl.glTexCoord2f(0, 0);
                    gl.glVertex2f(0, 0);
                gl.glEnd();

                gl.glBindTexture(GL2.GL_TEXTURE_2D, 0);

        }

        gl.glTranslatef(width / 2, -height / 2, 0);
        gl.glRotatef(rotation, 0, 0, 1);
        gl.glTranslatef(-x - width / 2 + Camera.getCamera().getLocation().x, y + height / 2 - Camera.getCamera().getLocation().y, 0);

    }

    public enum ObjectType {

        RECT,
        IMAGE;

    }

}
