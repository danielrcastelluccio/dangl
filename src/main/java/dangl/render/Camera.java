package dangl.render;

import dangl.update.Updatable;

public class Camera extends Updatable {

    private double x = 0;
    private double y = 0;
    private double rot = 0;

    private static Camera INSTANCE = new Camera();

    public static Camera getCamera() { return INSTANCE; }

    @Override
    public void update(double deltaTime) {

    }

    public void setCameraX(double x) {
        this.x = x;
    }

    public void setCameraY(double y) {
        this.y = y;
    }

    public void setCameraRotation(double rot) {
        this.rot = rot;
    }

    public Location getLocation() {
        return new Location(x, y, rot);
    }

}
