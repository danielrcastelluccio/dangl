package dangl.render;

public class Location {

    public float x = 0;
    public float y = 0;
    public float rot = 0;

    public Location(double x, double y, double rot) {
        this.x = (float) x;
        this.y = (float) y;
        this.rot = (float) rot;
    }

}
