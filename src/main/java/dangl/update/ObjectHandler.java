package dangl.update;

import java.util.ArrayList;

public class ObjectHandler {

    private static ArrayList<Updatable> updatables = new ArrayList<>();

    public static void addUpdatable(Updatable updatable) {
        updatables.add(updatable);
    }

    public static void removeUpdatable(Updatable updatable) {
        if(updatables.contains(updatable)) {
            updatables.remove(updatable);
        } else {
            //TODO: do
        }
    }

    public static void addObject(SceneObject object) {
        updatables.add(object);
    }

    public static ArrayList<Updatable> getUpdatables() { return updatables; }

}
