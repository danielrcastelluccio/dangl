package dangl.update;

public abstract class Updatable {

    public abstract void update(double deltaTime);

}
