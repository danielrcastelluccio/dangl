package dangl.update;

import dangl.DanGL;
import dangl.util.ValuePackage;
import dangl.display.Display;
import dangl.render.GLColor;
import dangl.render.ImageResource;
import dangl.render.RenderQueue;

import java.awt.*;
import java.util.ArrayList;

public class SceneObject extends Updatable {

    protected double x;
    protected double y;
    protected double width;
    protected double height;
    protected double rotation;
    protected double gravityEffect;
    protected Object object;
    private Display display;
    private boolean isAlive = true;

    private double verticalVelocity;
    private double horizontalVelocity;

    public SceneObject(double x, double y, double width, double height, double rotation, double gravityEffect, Object object) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.rotation = rotation;
        this.gravityEffect = gravityEffect;
        this.object = object;
        this.display = DanGL.getSettings().getMainDisplay();
    }

    public SceneObject(double x, double y, double width, double height, double rotation, double gravityEffect, Display display, Object object) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.rotation = rotation;
        this.gravityEffect = gravityEffect;
        this.object = object;
        this.display = display;
    }

    public void setImage(Object object) {
        this.object = object;
    }

    @Override
    public void update(double deltaTime) {

        if (this.gravityEffect > 0) {

            verticalVelocity += gravityEffect * (deltaTime / 10000000000L);

        }

        this.moveObject(0, verticalVelocity, 0);

    }

    public void moveObject(double deltaX, double deltaY, double deltaRotation) {

        double tempX = deltaX;
        double tempY = deltaY;
        double tempRot = deltaRotation;

        ArrayList<Updatable> updatables = ObjectHandler.getUpdatables();

        for(Updatable updatable : updatables) {

            Rectangle rectangle;

            if(updatable instanceof SceneObject && !updatable.equals(this)) {

                rectangle = this.getBounds();
                rectangle.y = rectangle.y + (int) deltaY;

                if(rectangle.intersects(((SceneObject) updatable).getBounds())) {
                    tempY = 0;
                    verticalVelocity = 0;
                }

                rectangle = this.getBounds();
                rectangle.x = rectangle.x + (int) deltaX;
                rectangle.y = rectangle.y - 1;

                if(rectangle.intersects(((SceneObject) updatable).getBounds())) {
                    tempX = 0;
                    horizontalVelocity = 0;
                }

            }
        }

        this.x += tempX;
        this.y += tempY;
        this.rotation += tempRot;

    }

    public void render() {
        if(object instanceof ImageResource) {
            RenderQueue.drawImage(x * display.getScaledWidth(), y * display.getScaledHeight(), width * display.getScaledWidth(), height * display.getScaledHeight(), rotation, (ImageResource) object);
        } else if(object instanceof GLColor) {
            RenderQueue.drawRect(x * display.getScaledWidth(), y * display.getScaledHeight(), width * display.getScaledWidth(), height * display.getScaledHeight(), rotation, (GLColor) object);
        }

    }

    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, (int) width, (int) height);
    }

    public void remove() {
        isAlive = false;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public ValuePackage getPosInfo() {
        return new ValuePackage(x, y, rotation);
    }

}
