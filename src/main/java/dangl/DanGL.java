package dangl;

import dangl.loop.LoopManager;
import dangl.render.Camera;
import dangl.render.Renderer;

public class DanGL {

    private static AbstractSettings gameSettings;
    private static Camera camera;

    private static final Renderer renderer = new Renderer();

    public static void begin() {
        try {
            LoopManager.graphicsLoop = DanGL.getSettings().getLoopClass().newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        LoopManager.wantedFPS = DanGL.getSettings().getTickRate();

        LoopManager.run();

    }

    public static Renderer getRenderer() { return renderer; }

    public static int getFPS() { return LoopManager.currentFPS; }

    public static void addSettings(AbstractSettings settings) { gameSettings = settings; }

    public static AbstractSettings getSettings() { return gameSettings; }

    public static Camera getCamera() {
        if(camera == null) {
            camera = new Camera();
        }

        return camera;
    }

}
