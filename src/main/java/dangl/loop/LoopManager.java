package dangl.loop;

import dangl.Logger;
import dangl.render.Camera;
import dangl.render.Renderer;
import dangl.update.SceneObject;
import dangl.update.Updatable;
import dangl.update.ObjectHandler;

import java.util.Iterator;

public class LoopManager {

    public static boolean running;

    public static AbstractLoop graphicsLoop;
    public static int wantedFPS;
    public static int currentFPS;

    public static long gameStartTime;

    public static void run() {

        running = true;

        if(graphicsLoop == null) {
            Logger.danWarn("No Graphics Method Class Specified");
        }

        Logger.danLog("Initializing...");
        if(graphicsLoop != null) { graphicsLoop.init(); }

        ObjectHandler.addUpdatable(Camera.getCamera());

        gameStartTime = System.nanoTime();

        long loopStartTime = System.nanoTime();
        long startedTime = System.nanoTime();
        int frames = 0;
        int seconds = 0;
        long deltaTime;

        Logger.danLog("Running...");

        while (running) {
            deltaTime = System.nanoTime() - loopStartTime;
            if(deltaTime > 1000000000L / wantedFPS) {
                loopStartTime = System.nanoTime();

                if (graphicsLoop != null) { graphicsLoop.update(deltaTime); }

                if (graphicsLoop != null) { graphicsLoop.render(); }

                Iterator iterator = ObjectHandler.getUpdatables().iterator();

                Updatable updatable;

                while(iterator.hasNext()) {
                    updatable = (Updatable) iterator.next();
                    updatable.update(deltaTime);
                    if (updatable instanceof SceneObject) {
                        ((SceneObject) updatable).render();
                        if(!((SceneObject) updatable).isAlive()) {
                            iterator.remove();
                        }
                    }
                }

                Renderer.render();

                frames++;
                if (((System.nanoTime() - startedTime) / 1000000000L) - seconds > 1) {
                    seconds++;
                    currentFPS = frames;
                    frames = 0;
                }
            } else {
                try {
                    Thread.sleep(1);
                } catch (Exception e) { }
            }
        }

        Logger.danLog("Exiting...");
        if(graphicsLoop != null) { graphicsLoop.exit(); }

        Runtime.getRuntime().exit(0);

    }

}
