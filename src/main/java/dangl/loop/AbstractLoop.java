package dangl.loop;

public abstract class AbstractLoop {

    public abstract void init();

    public abstract void update(double deltaTime);
    public abstract void render();

    public abstract void exit();

}
