package dangl.util;

public class Helpers {

    public static double getAngleBetweenPoints(double x1, double x2, double y1, double y2) {
        return Math.toDegrees(Math.atan2(y2 - y1, x2 - x1));
    }

    public static double getDistanceBetweenPoints(double x1, double x2, double y1, double y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

}
