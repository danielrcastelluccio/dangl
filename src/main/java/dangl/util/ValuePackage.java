package dangl.util;

public class ValuePackage {

    public Object[] values;

    public ValuePackage(Object... values) {
        this.values = values;
    }

    public Object get(int index) {
        return values[index];
    }

}
