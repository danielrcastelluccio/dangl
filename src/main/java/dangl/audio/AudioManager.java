package dangl.audio;

import dangl.util.Resource;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import java.io.*;
import java.util.ArrayList;

public class AudioManager {

    public static ArrayList<AudioStream> currentAudio = new ArrayList<>();

    public static void playSound(Resource resource) {
        try {
            InputStream music = new FileInputStream(resource.file);
            AudioStream audioStream = new AudioStream(music);
            AudioPlayer.player.start(audioStream);
            currentAudio.add(audioStream);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void stopSound(Resource resource) {

        AudioStream audio = null;

        try {
            InputStream music = new FileInputStream(resource.file);
            audio = new AudioStream(music);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for(AudioStream audioStream : currentAudio) {
            if(areFilesEqual(audio, audioStream)) {
                AudioPlayer.player.stop(audioStream);
                currentAudio.remove(audioStream);
            }
        }
    }

    public static boolean areFilesEqual(AudioStream a1, AudioStream a2) {

        boolean temp = true;

        if(a1.getLength() != a2.getLength()) {
            temp = false;
        }

        return temp;

    }

}
