package dangl;

public class Logger {

    private static final String danglPreface = "[DanGL/";
    private static final String userPreface = "[CLIENT/";
    public static LoggerMode loggerMode = LoggerMode.ALL;

    public static void log(String string) {
        if(loggerMode.user == true) {
            System.out.println(userPreface + "INFO] " + string);
        }
    }

    public static void err(String string) {
        System.out.println(userPreface + "ERR] " + string);
    }

    public static void warn(String string) {
        System.out.println(userPreface + "WARN] " + string);
    }

    public static void danLog(String string) {
        if(loggerMode.dangl == true) {
            System.out.println(danglPreface + "INFO] " + string);
        }
    }

    public static void danErr(String string) {
        System.out.println(danglPreface + "ERR] " + string);
    }

    public static void danWarn(String string) {
        System.out.println(danglPreface + "WARN] " + string);
    }

    public static void setMode(LoggerMode mode) {
        loggerMode = mode;
    }

    public enum LoggerMode {

        ALL(true, true),
        DANGLONLY(true, false),
        USERONLY(false, true),
        NONE(false, false);

        public boolean dangl;
        public boolean user;

        LoggerMode(boolean dangl, boolean user) {
            this.dangl = dangl;
            this.user = user;
        }

    }

}
