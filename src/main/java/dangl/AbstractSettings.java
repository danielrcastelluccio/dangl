package dangl;

import dangl.display.Display;
import dangl.loop.AbstractLoop;

public abstract  class AbstractSettings {

    public abstract Display getMainDisplay();

    public abstract Class<? extends AbstractLoop> getLoopClass();

    public abstract int getTickRate();

}
